<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

class Videos extends Module
{

    public function __construct()
    {
        $this->name = 'videos';
        $this->tab = 'content_management';
        $this->version = '1.0.0';
        $this->author = 'Piment bleu';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = [
            'min' => '1.6',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->trans('Videos');
        $this->description = $this->trans('Add videos in your products pages');
        $this->confirmUninstall = $this->trans('Are you sure you want to uninstall Videos ?');
    }

    public function getContent()
    {
        return 'Configuration';
    }

    public function install()
    {
        $this->registerHook('displayAdminProductsExtra');
        $this->registerHook('displayProductExtraContent');

        if (file_exists($this->local_path.'sql/install.php'))
        {
            include($this->local_path.'sql/install.php');
        } else {
            return false;
        }
        return parent::install();
    }

    public function uninstall()
    {
        $this->unregisterHook('displayAdminProductsExtra');
        $this->unregisterHook('displayProductExtraContent');

        if (file_exists($this->local_path.'sql/uninstall.php'))
        {
            include($this->local_path.'sql/uninstall.php');
        } else {
            return false;
        }
        return parent::uninstall();
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        $id_product = Tools::getValue('id_product');
        /*$video = Videos::loadByIdProduct($id_product);
        if(!empty($video) && isset($sampleObj->id)) {
            $this->context->smarty->assign(array(
                'textarea' => $sampleObj->textarea,
            ));
        }*/

        return $this->display(__FILE__, 'views/admin/sample.tpl');
    }

    public function hookActionProductUpdate($params)
    {
        $id_product = Tools::getValue('id_product');
        $video = Videos::loadByIdProduct($id_product);
        $video->textarea = Tools::getValue('belvg_sample');
        $video->id_product = $id_product;

        if(!empty($sampleObj) && isset($sampleObj->id)) {
            $video->update();
        } else {
            $video->add();
    }
        /*if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
            $id_product = (int)Tools::getValue('id_product');
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                SELECT * FROM `'. _DB_PREFIX_ .'video` v
                LEFT JOIN `' . _DB_PREFIX_ . 'video_product` vp ON v.id_product = vp.id_product'
            );
            $contents = array();
            foreach ($result as $key => $content) {
                $contents[$contents['id_lang']] = $contents['content'];
            }
            $this->context->smarty->assign(array(
                'video' => $contents,
                'languages' => Language::getLanguages(true),
                'default_language' => (int)Configuration::get('PS_LANG_DEFAULT'),
            ));
            return $this->display(__FILE__, 'views/templates/hook/displayAdminProductExtra.tpl');
        }*/
    }

    /*public function hookDisplayProductExtraContent($params)
    {
        $this->context->smarty->assign(array('id_product' =>$params['id_product']));
        $helper = new HelperForm();
        return $helper->generateForm(array(
            'form' => array(
                'legend' => array(
                    'title' => $this->getTranslator()->trans('Videos'),
                ),
                'input' => array(
                    array(
                        'type' => 'textarea',
                        'label' => $this->getTranslator()->trans('test'),
                        'name' => 'content'
                    ),
                ),
            ),
        ));
    }*/
    public function hookDisplayProductExtraContent($params)
    {
        $product = new Product((int)$params['id_product']);

        $array = array();
        $video = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                SELECT `name` FROM `'. _DB_PREFIX_ .'video_lang` vl
                WHERE id_video = 1;');
        foreach($video as $item) {

            $test = $item['name'];
        }
        $array[] = (new PrestaShop\PrestaShop\Core\Product\ProductExtraContent())
            ->setTitle('Vidéos')
            ->setContent('<iframe width="560" height="315" src="' . 'https://www.youtube.com/embed/pfNlfTRm5II' . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
    return $array;
    }
}