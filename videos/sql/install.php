<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

$sql = array(
    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'video`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'video` (
        `id_video` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `link_video` VARCHAR(255) DEFAULT NULL,
        PRIMARY KEY (`id_video`)
) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;',

    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'video_lang`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'video_lang` (
    `id_video` INT(11) UNSIGNED NOT NULL,
    `id_lang` INT(11) UNSIGNED NOT NULL,
    `name` VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (`id_video`,`id_lang`),
    KEY `id_lang` (`id_lang`,`name`)
) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;',

    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'video_product`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'video_product` (
    `id_video` INT(11) UNSIGNED NOT NULL,
    `id_product` INT(11) UNSIGNED NOT NULL,
    PRIMARY KEY (`id_video`,`id_product`),
    KEY `id_product` (`id_product`)
) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;',

    'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
    START TRANSACTION;
    SET time_zone = "+00:00";
    DROP TABLE IF EXISTS `'._DB_PREFIX_.'video_shop`;
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'video_shop` (
    `id_video` INT(11) UNSIGNED NOT NULL,
    `id_shop` INT(11) UNSIGNED NOT NULL,
    PRIMARY KEY (`id_video`,`id_shop`),
    KEY `id_shop` (`id_shop`)
) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    COMMIT;'
);

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}