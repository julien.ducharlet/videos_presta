<?php
// security : if the Prestashop constant (version number) does not exists => stops the module from loading
if (!defined('_PS_VERSION_')) {
    exit;
}

$sql = array();

$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'video`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'video_lang`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'video_product`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'video_shop`;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}